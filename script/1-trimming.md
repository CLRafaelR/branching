# data trimming

* 最終的に得たいデータ
    * 被験者番号
    * アイテム番号
    * Fixation座標
    * Fixationの時間幅 等

```csv
"ParticipantName","SegmentName","FixationPointX","FixationPointY","GazeStart","GazeEnd","hoge","piyo","ItemNo","Condition","AOI1","AOI2","AOI3","AOI4","AOI"
"P05","Segment 1",839,446,88,188,"1","3","6","d","A","D","C","B",1
"P05","Segment 1",927,449,215,368,"1","3","6","d","A","D","C","B",1
"P05","Segment 1",589,340,425,575,"1","3","6","d","A","D","C","B",1
```

* 一方、Tobiiから吐き出されるデータ

```tsv
ParticipantName SegmentName     SegmentStart    SegmentEnd      SegmentDuration RecordingTimestamp      StudioEvent     StudioEventData FixationIndex   SaccadeIndex    GazeEventType   GazeEventDuration       FixationPointX (MCSpx)  FixationPointY (MCSpx)  PupilLeft       PupilRight      
P05     Segment 1       51212   61655   10443   51212   SceneStarted    1 3 6 d A D C B         1       Saccade 63                                      
P05     Segment 1       51212   61655   10443   51213                           1       Saccade 63                      1.54    2.42    
P05     Segment 1       51212   61655   10443   51217                           1       Saccade 63                      1.70    2.00    
```

* TODO
    * 一つ一つのFixationの時間幅の特定
    * E-Prime上のStudioEventのデータを付与
        * condition, AOI情報など
    * x--y coordinate からAOIの特定

## ファイルリスト

パッケージの導入

```r
library(needs)
needs("reshape", "data.table", "dplyr", "magrittr")
"%+=%" = function(a,b) eval.parent(substitute(a=a+b))
"%-=%" = function(a,b) eval.parent(substitute(a=a-b))
"%+%"  = function(a,b) paste0(a,b)
```

処理を一度に行うためにファイルリストを作成

* 各ファイル名はproject＋test＋被験者＋segmentのパターン
* パターンを指定してリスト`data_list`に読込

```r
getwd()
home_dir = getwd()
# TODO: 適宜ディレクトリを変更
setwd(home_dir %+% "/result/data_main/tsv")

file_pattern = "BR2018_New test_Rec"
data_list = list.files(pattern=file_pattern)

if (length(data_list)==0){
    print('you might want to make some changes')
}else{
    print('loaded')
}
```

## fixationの入ってないデータを除外

```r
bad_trials = 0
for(file_name in data_list){
    trial=read.table(file_name, head=T, sep="\t",
                     na.string="NA", encoding="UTF-8")%>%
          subset(GazeEventType=="Fixation")
    if (nrow(trial) == 0){
        print("Bad trial!: " %+% file_name)
        bad_trials %+=% 1
    }
}
if (bad_trials == 0){
    print("there's no bad trial!")
}
```

## 全員分のデータを一つのファイルに集約（少し重い作業）

一つのファイルに対する作業

1. ファイル名からdata.frameを取得
1. data.frameから修正したdata.frameを取得
    * Timestamp追加
    * 不要カラム削除 
1. data.frameから同じFixationの時間帯を一つの行に集約
1. StudioEventDataの隣E-prime情報をマッピング
    * 試行回答と持続時間
    * src(source)ディレクトリのdurationを参照

### 必要な機能を作成

1. ファイル名からdata.frameを取得
    * 関数の動作確認の際に便利

```r
finename2dataframe = function(file_name){
    file_name %>% 
    read.table(head=T,sep="\t",
        na.string="NA",encoding="UTF-8") %>%
    as.data.frame %>%
    return
}
# 適用例
file_name = data_list[1] %>% print
file_name %>% finename2dataframe %>% head
```

2. data.frameから修正したdata.frameを取得
    * Timestamp追加
    * 不要カラム削除 

```r
refiner = function(raw){
    raw %>% 
    dplyr::select("ParticipantName", "SegmentName",
        "SegmentStart", "SegmentEnd", "SegmentDuration",
        "RecordingTimestamp", "FixationIndex", "SaccadeIndex",
        "GazeEventType", "GazeEventDuration",
        "FixationPointX..MCSpx.", "FixationPointY..MCSpx.",
        "PupilLeft", "PupilRight") %>%
    dplyr::rename(FixationPointX=FixationPointX..MCSpx.,
        FixationPointY=FixationPointY..MCSpx.) %>%
    dplyr::mutate(Timestamp=RecordingTimestamp-SegmentStart) %>%
    dplyr::select("ParticipantName", "SegmentName", "FixationIndex",
        "GazeEventType", "GazeEventDuration", "FixationPointX",
        "SaccadeIndex", "FixationPointY", "Timestamp") %>%
    dplyr::filter(GazeEventType != "Unclassified") %>%
    dplyr::mutate(FixationIndex =ifelse(FixationIndex  %>% is.na,
                  SaccadeIndex, FixationIndex)) %>%
    dplyr::mutate(FixationPointX=ifelse(FixationPointX %>% is.na,
                  -1,FixationPointX)) %>%
    dplyr::mutate(FixationPointY=ifelse(FixationPointY %>% is.na,
                  -1,FixationPointY)) %>%
    dplyr::mutate(SaccadeIndex=NULL)  %>%
    as.data.frame %>%
    return
}
file_name = data_list[1] %>% print
file_name %>% finename2dataframe %>% refiner %>% head
```

3. data.frameから同じFixationの時間帯を一つの行に集約

* aggregate の list を group by で代行
    * その集合に対してsummarise関数を適用
    * aggregateより柔軟でSQLライクな表記が可能
* 同Fixationの終了時間に対してmin/maxを適用
    * 一つのGazeIndexの開始時間と終了時間を算出

```r
addGazeFlag = function(refined_data){
    refined_data %>%
    dplyr::group_by(ParticipantName, SegmentName,
        FixationIndex, GazeEventType, GazeEventDuration,
        FixationPointX, FixationPointY) %>%
    dplyr::summarise(GazeStart=min(Timestamp),
        GazeEnd=max(Timestamp)) %>%
    as.data.frame %>%
    return
}

file_name = data_list[1] %>% print
file_name %>% finename2dataframe %>% 
    refiner %>% addGazeFlag %>% head
```

4. StudioEventDataの隣E-prime情報をマッピング

* 試行回答と持続時間
* srcディレクトリのdurationを参照
* E-primeからTobiiに送るられたStudioEventData

特定の値を持った行を繰り返してdataframeを取得する方法は[ここ](https://stackoverflow.com/questions/11121385/repeat-rows-of-a-data-frame)を参照

```r
addStudioEventData=function(data_with_gaze_flag,file_name,content){
    raw_data_frame = file_name %>% finename2dataframe
    list_of_eventdata = raw_data_frame[1,]$StudioEventData %>%
        as.character %>% strsplit(" ") %>% unlist()
    item_list    = list_of_eventdata[1]
    trial_number = list_of_eventdata[2]
    edata = content %>%
        filter(no  ==trial_number) %>% 
        filter(list==item_list)
    eprime_matrix = edata[rep(seq_len(nrow(edata)),
                          each=nrow(data_with_gaze_flag)),]
    data_with_gaze_flag %>% cbind(eprime_matrix) %>%
    as.data.frame %>%
    return
}
```

### 実行部分

* 実験デザイン時に分析に必要な項目を指定
* Tobiiから吐き出したtextファイルの中StudioEvent欄を回収
* 実験内容に応じてカラム数を変更

```r
data_all = NULL
content = read.csv("../src/content.csv")
for(n in 1:length(data_list)){
    print("now access to: "%+% n)
    data_with_eventdata =
        data_list[n] %>%
        finename2dataframe %>%
        refiner %>%
        addGazeFlag %>% 
        addStudioEventData(data_list[n],content)
    data_all %<>% rbind(data_with_eventdata)
}
```

## 注視点のXY座標をAOIにマッピングする

AOIにマッピングして保存

```r
# グローバル変数に data_all がある状態
# 1080*1920の画面を８分割
# 123
# 4 5
# 678
data_with_aoi = data_all %>% 
    dplyr::mutate(AOI=ifelse(
        FixationPointX >= 245 & FixationPointX <  725 &
        FixationPointY >= 0   & FixationPointY <  361 ,
        1, 0)) %>%
    dplyr::mutate(AOI = ifelse(
        FixationPointX >= 725 & FixationPointX < 1205&
        FixationPointY >= 0   & FixationPointY < 361,
        2, AOI))%>%
    dplyr::mutate(AOI = ifelse(
        FixationPointX >= 1205& FixationPointX < 1685&
        FixationPointY >= 0   & FixationPointY < 361,
        3, AOI))%>%
    dplyr::mutate(AOI = ifelse(
        FixationPointX >= 245 & FixationPointX < 725 &
        FixationPointY >= 361 & FixationPointY < 721,
        4, AOI))%>%
    dplyr::mutate(AOI = ifelse(
        FixationPointX >= 1205& FixationPointX < 1685&
        FixationPointY >= 361 & FixationPointY < 721,
        5, AOI))%>%
    dplyr::mutate(AOI = ifelse(
        FixationPointX >= 245 & FixationPointX < 725 &
        FixationPointY >= 721 & FixationPointY <= 1080,
        6, AOI))%>%
    dplyr::mutate(AOI = ifelse(
        FixationPointX >= 725 & FixationPointX < 1205&
        FixationPointY >= 721 & FixationPointY <= 1080,
        7, AOI))%>%
    dplyr::mutate(AOI = ifelse(
        FixationPointX >= 1205& FixationPointX < 1685&
        FixationPointY >= 721 & FixationPointY <= 1080,
        8, AOI)) %>%
    as.data.frame

data_with_aoi %>% head
data_with_fixation = 
    data_with_aoi %>% 
    dplyr::filter(GazeEventType=="Fixation")
# 必要のない情報を削除する。
data_with_fixation$GazeEventDuration = NULL
data_with_fixation$FixationIndex     = NULL
data_with_fixation$GazeEventType     = NULL

# 確認
head(data_with_fixation)

# データ全体のバランスを確認します。
table(data_with_fixation$ParticipantName, data_with_fixation$Condition)
table(data_with_fixation$ParticipantName, data_with_fixation$list)
# このsoundやsegmentの分布が美しくないが、後半はfillerであるため。
# FIXME: P38はなぜかfillerのデータも残ってしまっている。
table(data_with_fixation$ParticipantName, data_with_fixation$sound)
table(data_with_fixation$ParticipantName, data_with_fixation$SegmentName)
# csvで保存
write.csv(data_with_fixation, "../csv/output.csv", row.names=F)
```

* 以上のスクリプトでの変更が結果に影響していないことを保証

```sh
diff output.csv output-rev.csv 
```

