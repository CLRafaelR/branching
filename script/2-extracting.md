# 分析する区間を先に抜取

## ４つの分析開始区間に基づきデータを分割

「あおいネコのかさはどれ」における以下の４つのリージョンを可視化

 [1] "Word"       "beginword1" "endword1"   "durword1"   "beginword2"
 [6] "endword2"   "durword2"   "beginword3" "endword3"   "durword3"  
[11] "beginword4" "endword4"   "durword4"   "cnd"        "sound_name"

1. 2500msのバッファー
1. 最初の要素（あおい）の開始地点(word1)
1. 次の名詞（ネコ）の開始地点(word2)
1. 続く「の」の開始地点(word3)
1. ふたつめの名詞（かさ）の開始地点(word4)

## 具体的な処理

for文が重なってて怖いので関数に分けて対処

1. 初期設定（setwd(),library()）
1. output.csvとonset.csvを読込
1. onset.csvの**列名**のリストでfor文を回す
    1. isTargetOnset に onset である条件を指定します。
    1. もしonsetである条件をみたしているなら
        1. 現在のcolnameはonsetの条件を満たしています。
        1. 書き換えていくのでoutputという名前の変数に保管します。
        1. onsetのファイル内の上からしたまで以下の操作を実行
1. onset.csvファイルの行をfor文で回しi行目に以下の操作を実行
    1. まずは該当する行のsound（文字列:BBR01.wav）とonset(123ms)を取得
    1. onsetのsound_nameと同じ値を持つoutput.csvのrowsに対し
        1. subtract onset_value from GazeStart and GazeEnd from the outputs
    1. overwrite the output(名前はoutput-<colname>.csv).
    1. move on to i+1

## 実際のコード

```r
library(reshape)
library(data.table)
library(dplyr)
library(magrittr)
"%+=%" = function(a,b) eval.parent(substitute(a=a+b))
"%-=%" = function(a,b) eval.parent(substitute(a=a-b))
"%+%"  = function(a,b) paste0(a,b)

# 1. 初期設定
getwd()
setwd("/home/kishiyama/home/thesis/branching/result/data_main")

# 1. output.csvとonset.csvを読込
original_output   <- read.csv("./csv/output.csv", header =T)
sounds_and_onsets <- read.csv("./src/onset.csv", header =T)

original_output   %>% head
sounds_and_onsets %>% head

for (colname in colnames(sounds_and_onsets)) {
    isTargetOnset <- colname %>% startsWith("begin")
    if(isTargetOnset){
        print(colname)
        output <- original_output 
        colname <- colname
        for(index in sounds_and_onsets %>% nrow() %>% seq()){
            sound <- sounds_and_onsets[index,]["sound_name"][1][1,]
            onset <- sounds_and_onsets[index,][colname][1,]
            output[output$sound==sound,]$GazeStart =
                output[output$sound==sound,]$GazeStart - (onset+2500)
            output[output$sound==sound,]$GazeEnd =
                output[output$sound==sound,]$GazeEnd - (onset+2500)
        }
        output %>% write.csv("./csv/output-" %+% colname %+% ".csv", row.names=F)
    }else{
    # nothing to do
    }
}
```
