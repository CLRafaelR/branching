# 可視化

We are going to see the eye-movements
toward the target entities.

1. setting up (set wd, import libs, and read data)

```r
# install.packages('tidyverse')
library(tidyverse)
library(ggplot2)
library(reshape2)
library(plyr)
library(magrittr)

getwd()
setwd("/home/kishiyama/home/thesis/branching/result/data_main/csv")
head(list.files())
word1 = list.files()[1] %>% print
data_name = "word1"
data_all = read.csv(paste("./",word1,sep=""), header =T)
head(data_all)
```

1. digitalize the data

```r
# define the span for the graph here
span_begin = 0
span_end = 1000

# Generating a sequence from span_begin to span_end by 20 ms.
seq_span = seq(from=span_begin, to=span_end,20) %>% print()

# using matrix, make a table which has 
# the same number of row as data_all
# the same number of cols as seq-span
# with the same value as span_begin
# and rename the columns

binary_data = span_begin %>% 
    matrix(nrow=nrow(data_all), ncol=length(seq_span))
colnames(binary_data) = seq_span

# from span begin to end, check if there is gaze event
for (i in 1:length(seq_span)){
    binary_data[,i] = ifelse(
        (data_all$GazeStart < (span_begin + i * 20) &
        data_all$GazeEnd > (span_begin + i * 20)),
        1,
        0)}

# combine the binary data with all data
gr = data_all %>% cbind(binary_data %>% as.data.frame)　
gr %>% head

# Q2AOI1  AQ2OI2  Q2AOI3  Q4AOI4  AOI5    AOI6    AOI7    AOI8
# AOIs has numbers . see the content in the AOI.
gr$Target = ifelse(gr$AOI == 1, as.character(gr$Q2AOI1), "BackGround")
gr$Target = ifelse(gr$AOI == 2, as.character(gr$AQ2OI2), gr$Target)
gr$Target = ifelse(gr$AOI == 3, as.character(gr$Q2AOI3), gr$Target)
gr$Target = ifelse(gr$AOI == 4, as.character(gr$Q4AOI4), gr$Target)
gr$Target = ifelse(gr$AOI == 5, as.character(gr$AOI5), gr$Target)
gr$Target = ifelse(gr$AOI == 6, as.character(gr$AOI6), gr$Target)
gr$Target = ifelse(gr$AOI == 7, as.character(gr$AOI7), gr$Target)
gr$Target = ifelse(gr$AOI == 8, as.character(gr$AOI8), gr$Target)

# participant,item, cond, AOI,Target,bin
# 1000秒までだから、0--1000が20ms区切りで列になる。
# gazeのあるなしを0と1でしめしている。
# 同時に、Target列にはその時点で見つめている先が書いてあるので、
# 何をどのくらい見ていたかの情報がgrには入ることになる。
# ここまでは上手く行っている
# ちなみに、最初のセグメントで28のGazeEventがあったからしたは30にしてます。
gr= gr[,c(1, 8, 9, 24, ncol(gr), 25:(ncol(gr)-1))]
head(gr,30)

# melt time-binaries into one column.
gr2 = gr %>% reshape2::melt(id=c("ParticipantName", "sound", "Condition", "AOI", "Target"))

head(gr2)

# gr2$variable = gr2$variable %>% as.character %>% as.numeric
gr2$variable %<>% as.character %>% as.numeric
gr2 = gr2[order(gr2$ParticipantName, gr2$sound),]
gr2 %>% head(30)
gr2 %>% nrow

# in the recording of Tobii, count 1 if the AOI is seen in the 20m
# this causes many duplicates bacause of counting 0 for other AOIs.
# aggregate remove dups so that no dup in a bin
gr3 =aggregate(
    gr2$value,
    by = list(gr2$ParticipantName, gr2$sound, gr2$Condition, gr2$AOI, gr2$Target, gr2$variable),
    FUN = sum,
    na.rm = TRUE)
colnames(gr3) = c("subj","item","cond", "AOI", "variable", "bin","value")
gr3$AOI = NULL
gr3 = gr3[order(gr3$subj, gr3$item),]
gr3 %>% head(100)

# gr3 has 2 columns: `variable` and `value`
# function `cast` makes
# 1. new cols based on levels in `variable` 
# 1. new rows based on levels in `value`

gr.temp = gr3 %>% reshape2::dcast(subj + item + cond + bin ~ variable, sum)
gr.temp %>% head(10)

# gr.temp = gr3 %>% reshape2::dcast(subj + item + cond + bin ~ variable, sum)
#     subj      item cond bin BackGround DLB DRB TLB TRB filler
# 1   P35 BRD01.wav    D   0          0   0   0   0   0      0
# 2   P35 BRD01.wav    D  20          0   0   0   0   0      1
# 3   P35 BRD01.wav    D  40          0   0   0   0   0      1
# 4   P35 BRD01.wav    D  60          0   0   0   0   0      1

# 後世に残すべき教訓
# gr.temp = gr3 %>% cast
# > gr.temp %>% head(10)
#    subj      item cond bin BackGround DLB DRB TLB TRB filler
# 1   P35 BRD01.wav    D   0          1   1   1   1   1      3
# 2   P35 BRD01.wav    D  20          1   1   1   1   1      3
# 3   P35 BRD01.wav    D  40          1   1   1   1   1      3
# 4   P35 BRD01.wav    D  60          1   1   1   1   1      3

# If you need regard two(or more) area as one area,
# you might want to make some changes here.
# gr.temp$combined = gr.temp$DLB + gr.temp$DRB + gr.temp$TLB + gr.temp$TRB
head(gr.temp, 10)
# aggregate for graph (Use t1~t4)
# ここでaggregateする。
# 被験者やアイテムを平らに均す

gr.temp %<>% tidyr::gather(aoi, value, -subj, -item, -bin, -cond, convert=TRUE, factor_key=TRUE)

gra = aggregate(
    c(gr.temp$value),
    by=list(gr.temp$bin, gr.temp$cond, gr.temp$aoi),
    mean)

gra %>% head
colnames(gra) = c("bin", "cond", "aoi",  "mean")
gra = gra %>% tidyr::spread(aoi,mean)
delete = c(3,8)
gra = gra[,-delete]
head(gra)

# どちらかが0になると計算できなくなってしまう。
# 新井先生の論文では0でない最小値を出していた。
# data4ab$C が a と b にとっての正解。↲
# ここ、dat$wts<−1/(dat$AOI1+0.5)+1/(dat$sum−dat$AOI1+0.5)みたいにしたほうが良い。↲
gra$LogRatioTLBbyTRB <- log(
    ((gra$TLB) + 0.005)/
    ((gra$TRB) + 0.005))
gra$LogRatioDLBbyDRB <- log(
    ((gra$DLB) + 0.005)/
    ((gra$DRB) + 0.005))

head(gra)

delete = c(3,4,5,6)
gra = gra[,-delete]

gra %<>% tidyr::gather(aoi, value, -bin, -cond, factor_key=TRUE)

# gra$cond = gra$cond %>% revalue(c("D"="default", "R"="reset"))
gra$cond %<>% revalue(c("D"="default", "R"="reset"))
gra %>% head(500)

gra$bin %>% class
# 何故か一度文字に直さないと連番になってしまう。
gra$bin %<>% as.character %>% as.numeric()

gra = gra[order(gra$cond),]

# make a graph
# d = data.frame(t = c(4100, 4700 , 5500),
#           region = c("V1", "POL", "N2"))

gra$value %>% length
# gather の使い方に若干クセがある。
# factor key 周りの挙動を確認する必要あり。

name = paste("LogRatioTLBbyTRB_",data_name,sep="")
gra %>% subset(aoi=="LogRatioTLBbyTRB") %>% 
    ggplot(aes(x=bin, y=value,
    group=interaction(cond,aoi),
    linetype=cond, shape=cond, color=cond)) +
    geom_point(stat="identity",size=2) +
    geom_line(stat="identity",size=0.5) +
    scale_x_continuous("Time") +
    scale_y_continuous(limits=c(-3,3), name="empirical log ratio") +
    theme(axis.title.y = element_text(size = 16)) +
    theme(axis.title.x = element_text(size = 20)) +
    theme(legend.title = element_text(size = 16)) +
    theme(legend.text = element_text(size = 20))+
    theme_classic()+
    ggtitle(name)
# ここ、X11の設定と競合している。
# WSLの時は別ブランチ(master-wsl)のような形で管理が好ましそう
ppi = 600
# word1 = list.files()[1]
png(paste("../",name,".png",sep=""), width=6*ppi, height=6*ppi, res=ppi)
# dev.copy(pdf, paste("..//n2_v1_full.pdf")
dev.off()
```

